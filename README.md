# React Native Engineer Test

You are to complete the following task as part of your interview process for React/React Native engineer for Hashbangs Technologies Limited.

### Goal
Create a simple React/React-Native project to implement the provided interaction flow below. 

### UI/UX
The interaction below should serve as guide for what your final result should look like.
# 
![Click to view UI/UX](https://drive.google.com/uc?export=view&id=1ZKeTbTB5oa-XsSRFeoBYKhTgV5K_61q1)

### Submission
Share an application build with your final result (APK)
